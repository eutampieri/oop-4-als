#include <iostream>
#include <string>
#include "string_utilities.hpp"

using namespace std;

int main()
{
    string parola;
    cout << "Inserisci parola: ";
    cin >> parola;
    StringUtilities utils = StringUtilities(parola);
    if (utils.is_palyndrom())
    {
        cout << "La parola " << parola << " e' palindroma\n";
    }
}