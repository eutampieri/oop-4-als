//
//  string_utils.hpp
//  prova
//
//  Created by Eugenio Tampieri on 16/11/20.
//

#ifndef string_utils_h
#define string_utils_h

#include <string>
#include <cinttypes>

class StringUtilities
{
private:
    std::string store;

public:
    // Constructors
    StringUtilities(char *c_str)
    {
        store = c_str;
    }
    StringUtilities(std::string s)
    {
        store = s;
    }
    // Method for ex 02
    bool is_palyndrom()
    {
        size_t middle_point = store.size() / 2;
        for (size_t i = 0; i < middle_point; i++)
        {
            if (std::tolower(store[i]) != std::tolower(store[store.size() - i - 1]))
            {
                return false;
            }
        }
        return true;
    }
    // Method for ex 03
    bool are_braces_matching()
    {
        int open_curly = 0,
            open_square = 0,
            open_angular = 0,
            open_pars = 0;

        for (size_t i = 0; i < store.size(); i++)
        {
            switch (store[i])
            {
            case '{':
                open_curly++;
                break;
            case '[':
                open_square++;
                break;
            case '<':
                open_angular++;
                break;
            case '(':
                open_pars++;
                break;
            case '}':
                open_curly--;
                break;
            case ']':
                open_square--;
                break;
            case '>':
                open_angular--;
                break;
            case ')':
                open_pars--;
                break;
            }
        }
        return open_curly == 0 && open_square == 0 && open_angular == 0 && open_pars == 0;
    }
    // Destructor
    ~StringUtilities()
    {
        store.clear();
    }
};

#endif /* string_utils_h */
