#include <iostream>
#include <string>
using namespace std;

class Lampadina {
private:
    string stato;
    int numeroAccensioni;
    int numeroAccensioniMax;
public:
    Lampadina(int limit) {
        this->stato = "spenta";
        this->numeroAccensioni = 0;
        this->numeroAccensioniMax = limit;
    }
    string ottieniStato() {
        return this->stato;
    }
    void toggle() {
        if(this->numeroAccensioni == this->numeroAccensioniMax) {
            // Ho già raggiunto il numero massimo di cambi di stato, rompo la lampadina
            this->stato = "rotta";
        }

        if(this->stato == "rotta") {
            return;
        } else if(this->stato == "accesa") {
            this->stato = "spenta";
        } else {
            this->stato = "accesa";
        }
        this->numeroAccensioni++;
    }
};

int main() {
    string op = "";
    Lampadina l = Lampadina(10);
    while(op != "t" && l.ottieniStato() != "rotta") {
        cout << "La lampadina è " << l.ottieniStato() << ". Inserisci 't' per uscire dal programma o 'c' per cambiare stato alla lampadina.\nLa tua scelta: ";
        cin >> op;
        if(op == "c") {
            cout << "Cambio stato alla lampadina...\n";
            l.toggle();
        }
    }
    cout << "Esco dal programma. La lampadina è " << l.ottieniStato() << endl;
}
