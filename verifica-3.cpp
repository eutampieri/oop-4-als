#include <iostream>
#include <string>
using namespace std;

int main() {
    string csv_en_US, csv_it_IT;
    cout << "Inserisci la riga di CSV:\n";
    getline(cin, csv_en_US);
    for(int i = 0; i < csv_en_US.size(); i++) {
        if(csv_en_US[i] == '.') {
            csv_it_IT += ',';
        } else if(csv_en_US[i] == ',') {
            csv_it_IT += '.';
        } else {
            csv_it_IT += csv_en_US[i];
        }
    }
    cout << "La stringa CSV continentale è\n" << csv_it_IT << '\n';
    return 0;
}
