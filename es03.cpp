#include <iostream>
#include <string>
#include "string_utilities.hpp"

using namespace std;

int main()
{
    string parola;
    cout << "Inserisci parola: ";
    cin >> parola;
    StringUtilities utils = StringUtilities(parola);
    if (utils.are_braces_matching())
    {
        cout << "Le parentesi in " << parola << " combaciano\n";
    }
}