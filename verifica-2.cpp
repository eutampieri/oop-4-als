#include <iostream>
#include <string>
using namespace std;

// Funzione ricorsiva che elimina gli spazi da una stringa
string eliminaSpazi(string S) {
    /**Caso base, stringa di lunghezza 0
     * Ritorno la stringa
     */
    if(S.size() == 0) {
        return S;
    }
    // Sappiamo a questo punto che la stringa ha almeno un carattere
    if(S[0] == ' ') {
        // Passo ricorsivo, tolgo lo spazio (il primo carattere)
        // e ritorno la stringa senza spazi restituitami dalla fn
        // ricorsiva sulla sottostringa a partire dal 2° carattere
        return eliminaSpazi(S.substr(1, S.size() - 1));
    } else {
        // Passo ricorsivo, so che il primo carattere non è uno spazio
        // Concateno a questo carattere il risultato della chiamata a
        // eliminaSpazi sulla sottostringa a partire dal 2° carattere
        return S[0] + eliminaSpazi(S.substr(1, S.size() - 1));
    }
}

int main() {
    string s;
    cout << "Inserisci una stringa: ";
    getline(cin, s);
    cout << "Ho tolto tutti gli spazi da: \n\"" << s << "\"\ned e' diventata\n\"" << eliminaSpazi(s) << "\"\n";
}
