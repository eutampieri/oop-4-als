#include <iostream>
#include <cinttypes>

uint64_t T(uint64_t n)
{
    if (n == 0)
    {
        return 0;
    }
    else if (n == 1)
    {
        return 1;
    }
    else
    {
        return 2 * T(n - 2) + 5;
    }
}

int main()
{
    std::cout << "T(5) = " << T(5) << std::endl;
}