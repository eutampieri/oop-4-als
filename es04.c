#include <stdio.h>
#include <inttypes.h>

uint64_t ackermann(uint64_t m, uint64_t n) {
    if(m == 0) {
        return n+1;
    } else if(m > 0 && n == 0) {
        return ackermann(m-1, 1);
    } else {
        return ackermann(m-1, ackermann(m, n-1));
    }
}

int main() {
    printf("Inserisci m ed n: ");
    int n,m;
    scanf("%d %d", &m, &n);
    printf("ackermann(%d, %d) = %lld\n", m, n, ackermann(m,n));
    return 0;
}
